package com.costa.salvador.exercise1.validations;

import com.costa.salvador.exercise1.utils.validations.ValidNumber;
import org.junit.jupiter.api.Test;
import org.springframework.test.context.ActiveProfiles;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

@ActiveProfiles("test")
public class ValidNumberTests {

    ValidNumber validNumber = mock(ValidNumber.class);

    @Test
    void should_return_Truth_when_Divisible_By_Three() {
        var inputValue = 3;
        var expectedValue = true;

        when(validNumber.checkDivisibleByThree(inputValue)).thenReturn(true);

        assertEquals(expectedValue, validNumber.checkDivisibleByThree(inputValue));

        verify(validNumber, times(1)).checkDivisibleByThree(inputValue);

    }

    @Test
    void should_return_error_when_Divisible_By_Three() {
        var inputValue = 4;
        var expectedValue = false;

        when(validNumber.checkDivisibleByThree(inputValue)).thenReturn(false);

        assertEquals(expectedValue, validNumber.checkDivisibleByThree(inputValue));

        verify(validNumber, times(1)).checkDivisibleByThree(inputValue);

    }

    @Test
    void should_return_Truth_when_Divisible_By_Five() {
        var inputValue = 5;
        var expectedValue = true;

        when(validNumber.checkDivisibleByFive(inputValue)).thenReturn(true);

        assertEquals(expectedValue, validNumber.checkDivisibleByFive(inputValue));

        verify(validNumber, times(1)).checkDivisibleByFive(inputValue);

    }

    @Test
    void should_return_error_when_Divisible_By_Five() {
        var inputValue = 6;
        var expectedValue = false;

        when(validNumber.checkDivisibleByFive(inputValue)).thenReturn(false);

        assertEquals(expectedValue, validNumber.checkDivisibleByFive(inputValue));

        verify(validNumber, times(1)).checkDivisibleByFive(inputValue);

    }

    @Test
    public void should_return_Truth_when_Divisible_Both() {
        var inputValue = 15;
        var expectedValue = true;

        when(validNumber.checkDivisibleByBoth(inputValue)).thenReturn(true);

        assertEquals(expectedValue, validNumber.checkDivisibleByBoth(inputValue));

        verify(validNumber, times(1)).checkDivisibleByBoth(inputValue);

    }

    @Test
    void should_return_error_when_Divisible_Both() {
        var inputValue = 2;
        var expectedValue = false;

        when(validNumber.checkDivisibleByBoth(inputValue)).thenReturn(false);

        assertEquals(expectedValue, validNumber.checkDivisibleByBoth(inputValue));

        verify(validNumber, times(1)).checkDivisibleByBoth(inputValue);

    }

    @Test
    void should_return_Truth_when_null_number() {
        var inputValue = 2;
        var expectedValue = false;

        when(validNumber.isNull(inputValue)).thenReturn(false);

        assertEquals(expectedValue, validNumber.isNull(inputValue));

        verify(validNumber, times(1)).isNull(inputValue);

    }

    @Test
    void should_return_error_when_null_number() {
        Integer inputValue = null;
        var expectedValue = true;

        when(validNumber.isNull(inputValue)).thenReturn(true);

        assertEquals(expectedValue, validNumber.isNull(inputValue));

        verify(validNumber, times(1)).isNull(inputValue);

    }

}
