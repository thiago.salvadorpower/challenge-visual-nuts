package com.costa.salvador.exercise2.utils.exception;

public class ObjectNotFoundException extends RuntimeException {

    public ObjectNotFoundException() {
        super("Empty Result");
    }
}
