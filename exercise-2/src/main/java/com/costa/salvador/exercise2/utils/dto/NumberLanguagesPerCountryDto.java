package com.costa.salvador.exercise2.utils.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class NumberLanguagesPerCountryDto {

    private String country;
    private Integer numberLanguages;
}
