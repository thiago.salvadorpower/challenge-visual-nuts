package com.costa.salvador.exercise1.service;

import com.costa.salvador.exercise1.utils.enums.Number;
import com.costa.salvador.exercise1.utils.validations.ValidNumber;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.annotation.Bean;
import org.springframework.stereotype.Component;

import java.util.Locale;

@Component
public class ServiceProcessesDivisibleNumbers {

    private Logger logger = LoggerFactory.getLogger(ServiceProcessesDivisibleNumbers.class);

    @Autowired
    ValidNumber validNumber;

    @Autowired
    MessageSource messageSource;

    @Bean
    public void checkNumbersDivisibleBy3And5From1To100() {
        for (int number = Number.ONE.getValue(); number < Number.ONEHUNDREDONE.getValue(); number++) {
            logger.info(this.checkNumbers(number));
        }
    }

    public String checkNumbers(final Integer number) {

        if (validNumber.isNull(number)) {
           return messageSource.getMessage("number.null", null, Locale.ENGLISH);

        }
        if (validNumber.checkDivisibleByBoth(number)) {
            return messageSource.getMessage("number.divisible.by.both", null, Locale.ENGLISH);
        } else if (validNumber.checkDivisibleByThree(number)) {
            return messageSource.getMessage("number.divisible.by.tree", null, Locale.ENGLISH);
        } else if (validNumber.checkDivisibleByFive(number)) {
            return messageSource.getMessage("number.divisible.by.five", null, Locale.ENGLISH);
        }

        return String.valueOf(number);

    }
}


