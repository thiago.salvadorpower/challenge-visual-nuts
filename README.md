# Challenge Visual Nuts

## Goal

Demonstrate technical capacity as well as solve the proposed challenges

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

## Country API Services

- [Return all Countries in the world](http://20.125.106.80/country)

- [Returns the number of countries in the world](http://20.125.106.80/country/count)

- [Finds the country with the most official languages, where they officially speak German (de)](http://20.125.106.80/country/de)

- [That counts all the official languages spoken in the listed countries.](http://20.125.106.80/country/number-languages)

- [To find the country with the highest number of official languages.](http://20.125.106.80/country/greater-number-languages)

- [To find the most common official language(s), of all countries.](http://20.125.106.80/country/most-common-language)


## Tech

- [Java 11] - For backend
- [Mockito] - For unitary tests
- [BDD] - Behavior Driven Development
- [Spring reactive] - For work with reactive programming
- [Reactive Programming] - For leaves the backend with low response time
- [mongodb] - non-relational database
- [Docker] - For application container
- [Kubernetes] - For deploy app

## Main Design Patterns

- [DDD] Domain Driven design
- [S.O.L.I.D] Principle

## Going up application

Enter the Dockerfile File directory [/exercise-2] and issue the command below

```sh
docker build -t ms-countries .

docker run -d -p 8080:8080 ms-countries
```

Or

```sh
docker-compose up -d
```
Or directory any

```sh
docker container run -d -p 8080:8080 tsalvador/ms-countries:v1
```

