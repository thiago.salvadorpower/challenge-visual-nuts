package com.costa.salvador.exercise2.controller;

import com.costa.salvador.exercise2.entity.Countries;
import com.costa.salvador.exercise2.service.CountryService;
import com.costa.salvador.exercise2.utils.dto.NumberLanguagesPerCountryDto;
import com.costa.salvador.exercise2.utils.exception.ObjectNotFoundException;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.reactive.WebFluxTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.reactive.server.WebTestClient;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.List;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import static org.assertj.core.api.AssertionsForClassTypes.assertThatThrownBy;
import static org.mockito.Mockito.when;

//@WebMvcTest(controllers = CountryGetController.class)
@ExtendWith(SpringExtension.class)
@WebFluxTest(CountryGetController.class)
public class CountryGetControllerTest {

    @Autowired
    private WebTestClient webClient;

    @MockBean
    private CountryService service;

    @Test
    @DisplayName("")
    void should_return_number_countries() throws Exception {

        var result1=  Mono.just(5L);
        when(service.getNumberCountriesWorld()).thenReturn(result1);

        var result =webClient
                .get().uri("/country/count")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBody(Long.class)
                .consumeWith(System.out::println)
                .returnResult();

        Assertions.assertEquals(5L,result.getResponseBody());
    }

    @Test
    @DisplayName("")
    void should_return_exception_when_get_number_countries() throws Exception {

        when(service.getNumberCountriesWorld()).thenThrow( ObjectNotFoundException.class);

         webClient
                .get().uri("/country/count")
                .exchange()
                .expectStatus()
                .is5xxServerError()
                .expectBody(ObjectNotFoundException.class)
                .consumeWith(System.out::println)
                .returnResult();

        assertThatThrownBy(() -> service.getNumberCountriesWorld()).isInstanceOf(ObjectNotFoundException.class);

    }

    @Test
    @DisplayName("")
    void should_return_All_Countries() throws Exception {

        when(service.getAllCoutries()).thenReturn(getAllCoutriesMock());

        var result =webClient
                .get().uri("/country")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBodyList(Countries.class)
                .consumeWith(System.out::println)
                .returnResult();

        Assertions.assertEquals(getCountryList().size(), Objects.requireNonNull(result.getResponseBody()).size());

    }

    @Test
    @DisplayName("")
    void should_return_exception_when_get_all_Countries() throws Exception {

        when(service.getAllCoutries()).thenThrow( ObjectNotFoundException.class);

        var result =webClient
                .get().uri("/country")
                .exchange()
                .expectStatus()
                .is5xxServerError()
                .expectBodyList(Countries.class)
                .consumeWith(System.out::println)
                .returnResult();

        assertThatThrownBy(() -> service.getAllCoutries()).isInstanceOf(ObjectNotFoundException.class);

    }

    @Test
    @DisplayName("")
    void should_return_all_countries_speak_de() throws Exception {
        var language = "de";
        when(service.getAllCountriesSpeakDE(language)).thenReturn(getAllCoutriesMock().filter(data -> data.getLanguages().contains(language)));

        var expectResult = getCountryList().stream().filter(e -> e.getLanguages().contains(language)).collect(Collectors.toList());

        var result =webClient
                .get().uri("/country/de")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBodyList(Countries.class)
                .consumeWith(System.out::println)
                .returnResult();

        Assertions.assertEquals(expectResult, Objects.requireNonNull(result.getResponseBody()));
    }

    @Test
    @DisplayName("")
    void should_return_exception_when_get_all_countries_speak_de() throws Exception {
        var language = "de";
        when(service.getAllCountriesSpeakDE(language))
                .thenThrow( ObjectNotFoundException.class);

        var expectResult = getCountryList().stream().filter(e -> e.getLanguages().contains(language)).collect(Collectors.toList());

        var result =webClient
                .get().uri("/country/de")
                .exchange()
                .expectStatus()
                .is5xxServerError()
                .expectBodyList(Countries.class)
                .consumeWith(System.out::println)
                .returnResult();

        assertThatThrownBy(() -> service.getAllCountriesSpeakDE(language)).isInstanceOf(ObjectNotFoundException.class);
    }

    @Test
    @DisplayName("")
    void should_return_number_languages_by_country() throws Exception {

        when(service.getNumberOfLanguagesSpokenByCountries()).thenReturn(NumberLanguagesSpokenByCountriesDto());
        var expectResult = NumberLanguagesSpokenByCountriesListDto();

        var result =webClient
                .get().uri("/country/number-languages")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBodyList(NumberLanguagesPerCountryDto.class)
                .consumeWith(System.out::println)
                .returnResult();

        Assertions.assertEquals(expectResult, Objects.requireNonNull(result.getResponseBody()));
    }

    @Test
    @DisplayName("")
    void should_return_exception_when_get_number_languages_by_country() throws Exception {

        when(service.getNumberOfLanguagesSpokenByCountries()).thenThrow( ObjectNotFoundException.class);

        var result =webClient
                .get().uri("/country/number-languages")
                .exchange()
                .expectStatus()
                .is5xxServerError()
                .expectBodyList(NumberLanguagesPerCountryDto.class)
                .consumeWith(System.out::println)
                .returnResult();

        assertThatThrownBy(() -> service.getNumberOfLanguagesSpokenByCountries()).isInstanceOf(ObjectNotFoundException.class);
    }

    @Test
    @DisplayName("")
    void should_return_country_most_spoken_language() throws Exception {
        var country = "BE";
        when(service.getCountrySpeakMostLanguages()).thenReturn(getAllCoutriesMock().filter(e -> e.getCountry().contains(country)));
        var expectResult = getCountryList().stream().filter(e -> e.getCountry().contains(country)).collect(Collectors.toList());

        var result =webClient
                .get().uri("/country/greater-number-languages")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBodyList(Countries.class)
                .consumeWith(System.out::println)
                .returnResult();

        Assertions.assertEquals(expectResult, Objects.requireNonNull(result.getResponseBody()));
    }

    @Test
    @DisplayName("")
    void should_return_exception_when_get_country_most_spoken_language() throws Exception {
        var country = "BE";
        when(service.getCountrySpeakMostLanguages()).thenThrow( ObjectNotFoundException.class);
                webClient
                .get().uri("/country/greater-number-languages")
                .exchange()
                .expectStatus()
                .is5xxServerError()
                .expectBodyList(Countries.class)
                .consumeWith(System.out::println)
                .returnResult();

        assertThatThrownBy(() -> service.getCountrySpeakMostLanguages()).isInstanceOf(ObjectNotFoundException.class);
    }

    @Test
    @DisplayName("")
    void should_return_the_most_common_language_across_countries() throws Exception {
        var commonLanguages = "de";
        when(service.geTheMostCommonLanguageAcrossCountries()).thenReturn(commonLanguages);
        var expectResult=List.of(commonLanguages);

        var result =webClient
                .get().uri("/country/most-common-language")
                .exchange()
                .expectStatus()
                .isOk()
                .expectBodyList(String.class)
                .consumeWith(System.out::println)
                .returnResult();

        Assertions.assertEquals(expectResult, Objects.requireNonNull(result.getResponseBody()));
    }

    @Test
    @DisplayName("")
    void should_return_exception_when_get_the_most_common_language_across_countries() throws Exception {
        var commonLanguages = "de";
        when(service.geTheMostCommonLanguageAcrossCountries()).thenThrow( ObjectNotFoundException.class);
        var expectResult=List.of(commonLanguages);

        var result =webClient
                .get().uri("/country/most-common-language")
                .exchange()
                .expectStatus()
                .is5xxServerError()
                .expectBodyList(String.class)
                .consumeWith(System.out::println)
                .returnResult();

        assertThatThrownBy(() -> service.geTheMostCommonLanguageAcrossCountries()).isInstanceOf(ObjectNotFoundException.class);
    }

    public Flux<Countries> getAllCoutriesMock() {
        Countries c1 = Countries.builder()
                .country("NL")
                .languages(Set.of("fy","nl"))
                .build();
        Countries c2 = Countries.builder()
                .country("US")
                .languages(Set.of("en"))
                .build();
        Countries c3 = Countries.builder()
                .country("BE")
                .languages(Set.of("de","fr","nl"))
                .build();
        Countries c4 = Countries.builder()
                .country("DE")
                .languages(Set.of("de"))
                .build();
        Countries c5 = Countries.builder()
                .country("ES")
                .languages(Set.of("es"))
                .build();

        Flux<Countries> countries = Flux.just(c1,c2,c3,c4,c5);

        return countries;

    }

    public List<Countries> getCountryList() {

        return List.of(
        Countries.builder()
                .country("NL")
                .languages(Set.of("fy","nl"))
                .build(),
        Countries.builder()
                .country("US")
                .languages(Set.of("en"))
                .build(),
         Countries.builder()
                .country("BE")
                .languages(Set.of("fr", "de", "nl"))
                .build(),
        Countries.builder()
                .country("DE")
                .languages(Set.of("de"))
                .build(),
        Countries.builder()
                .country("ES")
                .languages(Set.of("es"))
                .build());

    }

    public Flux<NumberLanguagesPerCountryDto> NumberLanguagesSpokenByCountriesDto() {
        NumberLanguagesPerCountryDto c1 = NumberLanguagesPerCountryDto.builder().country("NL").numberLanguages(2).build();
        NumberLanguagesPerCountryDto c2 = NumberLanguagesPerCountryDto.builder().country("US").numberLanguages(1).build();
        NumberLanguagesPerCountryDto c3 = NumberLanguagesPerCountryDto.builder().country("BE").numberLanguages(3).build();
        NumberLanguagesPerCountryDto c4 = NumberLanguagesPerCountryDto.builder().country("DE").numberLanguages(1).build();
        NumberLanguagesPerCountryDto c5 = NumberLanguagesPerCountryDto.builder().country("ES").numberLanguages(1).build();

        Flux<NumberLanguagesPerCountryDto> countriesFluxDto = Flux.just(c1, c2, c3, c4, c5);

        return countriesFluxDto;
    }

    public List<NumberLanguagesPerCountryDto> NumberLanguagesSpokenByCountriesListDto(){
        return List.of(
                NumberLanguagesPerCountryDto.builder().country("NL").numberLanguages(2).build(),
                NumberLanguagesPerCountryDto.builder().country("US").numberLanguages(1).build(),
                NumberLanguagesPerCountryDto.builder().country("BE").numberLanguages(3).build(),
                NumberLanguagesPerCountryDto.builder().country("DE").numberLanguages(1).build(),
                NumberLanguagesPerCountryDto.builder().country("ES").numberLanguages(1).build());
    }

}
