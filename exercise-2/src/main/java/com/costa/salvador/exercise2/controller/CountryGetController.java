package com.costa.salvador.exercise2.controller;

import com.costa.salvador.exercise2.entity.Countries;
import com.costa.salvador.exercise2.service.CountryService;
import com.costa.salvador.exercise2.utils.dto.NumberLanguagesPerCountryDto;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

@RestController
@RequestMapping("/country")
public class CountryGetController {

    private Logger logger = LoggerFactory.getLogger(CountryGetController.class);

    @Autowired
    private CountryService service;

    @GetMapping()
    @ResponseBody
    public ResponseEntity<Flux<Countries>> getAllCoutries() {
        var result = service.getAllCoutries();
        logger.info("output data {}", result);
        return new ResponseEntity<> (result, HttpStatus.OK);
    }

    @GetMapping("/count")
    @ResponseBody
    public ResponseEntity<Mono<Long> >count() {
        var result = service.getNumberCountriesWorld();
        logger.info("output data {}", result);
        return new ResponseEntity<> (result, HttpStatus.OK);
    }

    @GetMapping("/de")
    public ResponseEntity<Flux<Countries>> getNumberCountryLanguageDE() {
        var result = service.getAllCountriesSpeakDE("de");
        logger.info("output data {}", result);
        return new ResponseEntity<> (result, HttpStatus.OK);
    }

    @GetMapping("/number-languages")
    public ResponseEntity<Flux<NumberLanguagesPerCountryDto>> getNumberLanguagesByCountry() {
        var result = service.getNumberOfLanguagesSpokenByCountries();
        logger.info("output data {}", result);
        return new ResponseEntity<> (result, HttpStatus.OK);
    }

    @GetMapping("/greater-number-languages")
    public ResponseEntity<Flux<Countries> >getCountryMostSpokenLanguage() {
        var result = service.getCountrySpeakMostLanguages();
        logger.info("output data {}", result);
        return new ResponseEntity<> (result, HttpStatus.OK);

    }

    @GetMapping("/most-common-language")
    public ResponseEntity<String> geTheMostCommonLanguageCountries() {
        var result = service.geTheMostCommonLanguageAcrossCountries();
        logger.info("output data {}", result);
        return new ResponseEntity<> (result, HttpStatus.OK);
    }
}
