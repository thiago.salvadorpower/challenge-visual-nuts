package com.costa.salvador.exercise1.utils.validations;


import org.springframework.stereotype.Component;

@Component
public class ValidNumber {

    public boolean checkDivisibleByThree(final Integer number) {
        return number % 3 == 0;
    }

    public boolean checkDivisibleByFive(final Integer number) {
        return number % 5 == 0;
    }

    public boolean isNull(final Integer number) {
        return number == null;
    }

    public boolean checkDivisibleByBoth(final Integer number) {
        return checkDivisibleByFive(number) && checkDivisibleByThree(number);
    }


}
