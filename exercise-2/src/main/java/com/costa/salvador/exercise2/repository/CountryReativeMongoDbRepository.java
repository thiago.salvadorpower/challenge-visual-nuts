package com.costa.salvador.exercise2.repository;

import com.costa.salvador.exercise2.entity.Countries;
import org.springframework.data.mongodb.repository.ReactiveMongoRepository;
import org.springframework.stereotype.Repository;
import reactor.core.publisher.Mono;

@Repository
public interface CountryReativeMongoDbRepository extends ReactiveMongoRepository<Countries,String> {

    @Override
    Mono<Long> count();



}

