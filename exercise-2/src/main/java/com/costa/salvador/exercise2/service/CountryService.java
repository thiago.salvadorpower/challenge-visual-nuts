package com.costa.salvador.exercise2.service;

import com.costa.salvador.exercise2.entity.Countries;
import com.costa.salvador.exercise2.repository.CountryReativeMongoDbRepository;
import com.costa.salvador.exercise2.utils.dto.NumberLanguagesPerCountryDto;
import com.costa.salvador.exercise2.utils.exception.ObjectNotFoundException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.Comparator;
import java.util.HashMap;
import java.util.Map;
import java.util.NoSuchElementException;


@Service
public class CountryService {

    private Logger logger = LoggerFactory.getLogger(CountryService.class);
    @Autowired
    private CountryReativeMongoDbRepository repository;


    public Mono<Long> getNumberCountriesWorld() {
        return repository.count();
    }

    public Flux<Countries> getAllCoutries() {
        return repository.findAll();
    }

    public Flux<Countries> getAllCountriesSpeakDE(String language) {
        var resulCountries = repository.findAll();
        return resulCountries
                .filter(data -> data.getLanguages().contains(language));

    }

    public Flux<NumberLanguagesPerCountryDto> getNumberOfLanguagesSpokenByCountries() {
        var resulCountries = repository.findAll();
        return resulCountries.mapNotNull(temp -> {
            NumberLanguagesPerCountryDto dto = new NumberLanguagesPerCountryDto();
            dto.setCountry(temp.getCountry());
            dto.setNumberLanguages(temp.getLanguages().size());
            return dto;
        }).switchIfEmpty(Flux.error(ObjectNotFoundException::new));
    }

    public Flux<Countries> getCountrySpeakMostLanguages() {

        var resulCountries = getNumberOfLanguagesSpokenByCountries();

        var resulLanguagesCountryDto = resulCountries
                .toStream()
                .max(Comparator.comparing(NumberLanguagesPerCountryDto::getNumberLanguages))
                .orElseThrow(NoSuchElementException::new);

        return repository.findAll()
                .filter(data -> data.getCountry().contains(resulLanguagesCountryDto.getCountry()))
                .switchIfEmpty(Flux.error(ObjectNotFoundException::new));
    }


    public String geTheMostCommonLanguageAcrossCountries() {

        var resulCountries = repository.findAll().switchIfEmpty(Flux.error(ObjectNotFoundException::new));
        Map<String, Integer> mapOccurence = new HashMap<String, Integer>();

        resulCountries.toStream().map(Countries::getLanguages).forEach(e -> {
            for (Object stringValue : e.toArray()) {
                String language = stringValue.toString();
                if (mapOccurence.containsKey(language)) {
                    mapOccurence.put(language, mapOccurence.get(language) + 1);
                } else {
                    mapOccurence.put(language, 1);
                }
            }
        });

        return mapOccurence.isEmpty() ? null : mapOccurence.entrySet()
                .stream()
                .sorted(Map.Entry.comparingByValue(Comparator.reverseOrder()))
                .iterator()
                .next()
                .getKey();

    }

}
