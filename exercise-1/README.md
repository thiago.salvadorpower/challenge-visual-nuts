# Challenge Visual Nuts

## Goal

Demonstrate technical capacity as well as solve the proposed challenges

[![Build Status](https://travis-ci.org/joemccann/dillinger.svg?branch=master)](https://travis-ci.org/joemccann/dillinger)

## Exercise 1 :

Write or describe an algorithm that prints the whole integer numbers to the console, start
from the number 1, and print all numbers going up to the number 100.
However, when the number is divisible by 3, do not print the number, but print the word
'Visual'. If the number is divisible by 5, do not print the number, but print 'Nuts'. And for all
numbers divisible by both (eg: the number 15) the same, but print 'Visual Nuts'.

### About Question

How will you keep this code safe from bugs? Show how you would guarantee that this code
keeps working when developers start making small feature adjustments.

Resp.
It can be solved with unit tests! tying the methods, in case of change in the code, the tests must fail and with that the developer must first understand the code and then change.

## Tech

- [Java 11] - For backend
- [Mockito] - For unitary tests
- [DDD] - Behavior Driven Development
- [Spring] - For work with reactive programming

## Main Design Patterns

- [DDD] Domain Driven design
- [S.O.L.I.D] Principle


