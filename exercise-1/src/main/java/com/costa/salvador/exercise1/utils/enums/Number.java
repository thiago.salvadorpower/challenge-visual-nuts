package com.costa.salvador.exercise1.utils.enums;

public enum Number {
    ONE(1), 
    ONEHUNDREDONE(101),

    TREE(3),

    FOUR(4),

    FIVE(5),

    FIFTEEN(15);

    private final int values;

    Number(int Value) {
        values = Value;
    }

    public int getValue() {
        return values;
    }

}
