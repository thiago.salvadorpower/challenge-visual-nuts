package com.costa.salvador.exercise1.service;

import com.costa.salvador.exercise1.utils.enums.Number;
import org.junit.jupiter.api.Test;
import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.mockito.Mockito.*;

class ServiceProcessesDivisibleNumbersTests {
    private ServiceProcessesDivisibleNumbers service = mock(ServiceProcessesDivisibleNumbers.class);

    @Test
    void method_call_one_time_expect_success() {
        service.checkNumbersDivisibleBy3And5From1To100();
        verify(service).checkNumbersDivisibleBy3And5From1To100();

    }

    @Test
    void should_return_visual_message() {
        var inputValue = Number.TREE.getValue();
        var expectedValue = "Visual!";

        when(service.checkNumbers(inputValue)).thenReturn(expectedValue);

        assertEquals(expectedValue, service.checkNumbers(inputValue));

        verify(service, times(1)).checkNumbers(inputValue);
    }

    @Test
    void should_return_nozes_message() {
        var inputValue = Number.FIVE.getValue();
        var expectedValue = "Nozes!";

        when(service.checkNumbers(inputValue)).thenReturn(expectedValue);

        assertEquals(expectedValue, service.checkNumbers(inputValue));

        verify(service, times(1)).checkNumbers(inputValue);
    }

    @Test
    void should_return_visual_nutus_message() {
        var inputValue = Number.FIFTEEN.getValue();
        var expectedValue = "Visual Nuts!";

        when(service.checkNumbers(inputValue)).thenReturn(expectedValue);

        assertEquals(expectedValue, service.checkNumbers(inputValue));

        verify(service, times(1)).checkNumbers(inputValue);
    }

    @Test
    void should_return_any_value_message() {
        var inputValue = Number.FOUR.getValue();
        var expectedValue = String.valueOf(inputValue);

        when(service.checkNumbers(inputValue)).thenReturn(expectedValue);

        assertEquals(expectedValue, service.checkNumbers(inputValue));

        verify(service, times(1)).checkNumbers(inputValue);
    }


}
